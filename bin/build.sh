#!/bin/sh

set -e

function finish {
  echo "docker logout ..."
  docker.logout
}
trap finish EXIT

base=$(readlink -nf $(dirname $(dirname "$0")))

echo "Project-Directory=${base}"
echo "Task=${TASK}"

echo "check arch"
if [ "${imgarch}" = "arm" ]; then
  buildarch="arm32v7/"
elif [ ! "${imgarch}" = "amd64" ]; then
  echo "unsupported image arch: ${imgarch}"
  exit 1
fi

if [ ! "${CI_COMMIT_BRANCH}" == "master" ]; then
  dev=_dev
fi;

echo "set job-specific variables"
if [ "${TASK}" = "artifacts" ]; then
  echo "building artifacts ..."
  dockerfile="Dockerfile-builder"
  dest="--destination ${CI_REGISTRY_IMAGE}/alexa_sdk:${SDK_Version}_artifacts${dev}_${imgarch}"
  buildarg="--single-snapshot --build-arg buildarch=${buildarch} --build-arg SDK_Version=${SDK_Version} --build-arg CURL_Version=${CURL_Version} --build-arg KittAI_Version=${KittAI_Version} --build-arg Sensory_Version=${Sensory_Version} --build-arg BUILDTYPE=MINSIZEREL"
else
  echo "building runtime ..."
  dockerfile="Dockerfile-run"
  dest="--destination ${CI_REGISTRY_IMAGE}/alexa_sdk:${SDK_Version}${dev}_${imgarch}"
  buildarg="--build-arg buildarch=${buildarch}"

  builderimg="${CI_REGISTRY_IMAGE}/alexa_sdk:${SDK_Version}_artifacts${dev}_${imgarch}"
  sed -i "s|\${builderimg}|"${builderimg}"|g" $CI_PROJECT_DIR/build/Dockerfile-run
fi

echo ""
echo "Dockerfile=${dockerfile}"
echo "Destination=${dest}"
echo "Build-Args=${buildarg}"

docker.gitlab.login

build --context ${base}/build \
  --dockerfile ${base}/build/${dockerfile} \
  ${dest} \
  ${buildarg}

if [ "${TASK}" = "runtime" ]; then
  if [ "${CI_COMMIT_BRANCH}" == "master" ]; then
    docker.manifest ${CI_REGISTRY_IMAGE}/alexa_sdk:${SDK_Version} latest
    docker.manifest ${CI_REGISTRY_IMAGE}/alexa_sdk:${SDK_Version} ${SDK_Version}
    docker.sync "${CI_REGISTRY_IMAGE}/alexa_sdk:${SDK_Version} ${CI_REGISTRY_IMAGE}/alexa_sdk:latest"
  else
    docker.manifest ${CI_REGISTRY_IMAGE}/alexa_sdk:${SDK_Version}_dev ${SDK_Version}_dev
    docker.sync "${CI_REGISTRY_IMAGE}/alexa_sdk:${SDK_Version}_dev"
  fi
fi
